#include <math.h>
#include <stdio.h>

int main() {
    int i,j;
    float z;
    FILE *fp;
    float degtorad=3.1415/180.;
    fp = fopen("topo_data.dat","w");
    for (i=70; i<181; i++){
        for (j=-30; j<60; j++){
            z = sin(2*i*degtorad)*cos(8*j*degtorad);
            fprintf(fp,"%d %d %f\n",i,j,z);
        }
    }
}
