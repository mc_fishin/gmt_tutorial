#!/bin/bash
#################################################
#Plot map with beachball diagrams 
#################################################
#Bounding region west/east/south/north
region=80/160/-40/40 
#Scale is Mercator projection 4 inches wide
scale=M4i
outfile=beachball_map
beach_file=datfiles/beachballs.dat

#Annotate every 10th degree. Only display labels on West and South of axies
#-A10000 does not plot lakes/rivers with features less than 10000 km^2
#Make the water white and the land lightgrey
pscoast -R$region -J$scale -B10 -BWSne -A10000 -Swhite -Glightgrey -K > $outfile.ps

#First way to plot beachballs:with strike dip slip
# Use -Sc
#-S adjusts radius of beachball which is proportinal to magnitude.
# -h1 skips first line, or header.
gmt psmeca -R$region -J$scale -Sc1.0 -h1 -K -O << END >> $outfile.ps 
lon lat depth str dip slip st dip slip mant exp plon plat
100 0 12. 180 18 -88 0 72 -90 5.5 0 0 0
100 20 12. 180 18 -88 0 72 -90 5.5 0 0 0
END

#Second way to plot beachballs: with CMTSOLUTION file info
#Use sed and awk to strip information from CMTSOLUTION file
lat=$(sed '5q;d' datfiles/CMTSOLUTION | awk '{print $2}')
lon=$(sed '6q;d' datfiles/CMTSOLUTION | awk '{print $2}')
dep=$(sed '7q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mrr=$(sed '8q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mtt=$(sed '9q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mpp=$(sed '10q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mrt=$(sed '11q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mrp=$(sed '12q;d' datfiles/CMTSOLUTION | awk '{print $2}')
mtp=$(sed '13q;d' datfiles/CMTSOLUTION | awk '{print $2}')
#exponent is 1 because I'm reading the exponent from the CMT file
e=1

#Make a red and blue beachball
psmeca -R$region -J$scale -Sm0.5 -Ered -Gblue -K -O << END >> $outfile.ps
$lon $lat $dep $mrr $mtt $mpp $mrt $mrp $mrp $e $lon $lat
END

#Third way is to use bash to read a file with multiple entries
while read lat lon dep mrr mtt mpp mrt mrp mtp e lon lat; do
psmeca -R$region -J$scale -Sm0.5 -Ered -Gblue -K -O << END >> $outfile.ps
$lon $lat $dep $mrr $mtt $mpp $mrt $mrp $mrp $e $lon $lat
END
done < datfiles/list_of_events

evince $outfile.ps




