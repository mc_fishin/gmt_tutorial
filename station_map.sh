#!/bin/bash
boxregion=80/150/-10/50 
region=70/160/-20/60 
scale=M3.0i
#scale=m0.04i
file=station_map
china_stations=datfiles/stations.dat
grid_stations=grid_stations.dat
#stations=datfiles/obspydmt_statcoords.dat
beach_file=datfiles/beachballs.dat
#gmtset FONT_LABEL=8

pscoast -R$region -J$scale -Ba10 -Swhite -G#CC9933 -A10000 -K  > $file.ps
gmt psxy $grid_stations -R$region -J$scale -Si0.08c -Gblack -K -O >> $file.ps

##############################################MECA
while read lat lon H mrr mtt mpp mrt mrp mtp e lon lat
do
psmeca -R$region -J$scale -Sm0.035i -Cred -K -O << EOF >> $file.ps
$lon $lat $H $mrr $mtt $mpp $mrt $mrp $mtp $e $lon $lat
EOF
done < $beach_file
##############################################MECA

pscoast -R$region -J$scale -Ba10 -Swhite -G#CC9933 -A10000 -K -O -X4.0i >> $file.ps
gmt psxy $china_stations -R$region -J$scale -Si0.08c -Gblack -K -O >> $file.ps

##############################################MECA
while read lat lon H mrr mtt mpp mrt mrp mtp e lon lat
do
psmeca -R$region -J$scale -Sm0.035i -K -O << EOF >> $file.ps
$lon $lat $H $mrr $mtt $mpp $mrt $mrp $mtp $e $lon $lat
EOF
done < $beach_file
##############################################MECA

ps2pdf $file.ps
convert -density 150 $file.pdf -quality 90 $file.png
rm $file.ps
evince $file.pdf &> /dev/null



